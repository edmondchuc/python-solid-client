import os
import unittest
import uuid
import io
import httpx

from solid.solid_api import SolidAPI, FolderData

POD_ENDPOINT = os.environ.get('SOLID_ENDPOINT', 'http://localhost:3000/')


def gen_random_str() -> str:
    return uuid.uuid4().hex


class MyTestCase(unittest.TestCase):
    def setUp(self):
        # Preflight check
        check_solid_online = httpx.get(POD_ENDPOINT)
        self.assertEqual(check_solid_online.status_code, 200, f'Is Solid OIDC running on {POD_ENDPOINT}?')

    def test_crud(self):
        api = SolidAPI()

        self._test_crud(api)

    def test_api_wrapper(self):
        from openidc_client import OpenIDCClient
        from oic.oic import Client as OicClient
        from oic.utils.authn.client import CLIENT_AUTHN_METHOD
        from solidclient.solid_client import SolidAPIWrapper
        import jwcrypto.jwk

        # OID Provider information
        provider_info = httpx.get(POD_ENDPOINT + ".well-known/openid-configuration").json()

        # Register client with OID
        registration_response = OicClient(
            client_authn_method=CLIENT_AUTHN_METHOD).register(provider_info.get('registration_endpoint'),
                                                              redirect_uris=['http://localhost:12345/'])

        client_id = registration_response['client_id']
        client_secret = registration_response['client_secret']

        client = OpenIDCClient("user", POD_ENDPOINT,
                               {"Authorization": "idp/auth", "Token": "idp/token"},
                               client_id=client_id,
                               client_secret=client_secret,
                               use_pkce=True)

        scopes = ["openid", "offline_access"]
        # gets Bearer access token as an example, usage of DPoP tokens is not illustrated here
        # alternatively, pass an existing full DPoP access_token received from OIDC after exchanging
        # authorization code for DPoP access token
        client.get_token(scopes)

        # get the whole token from internal cache
        access_token = client._get_token_with_scopes(tuple(scopes))[1]

        # keys used for DPoP verification
        keypair = jwcrypto.jwk.JWK.generate(kty='EC', crv='P-256')

        # wrapper around SolidAPI, injects given access_token to OpenIDCClient with _add_token()
        api = SolidAPIWrapper(client_id=client_id,
                              client_secret=client_secret,
                              access_token=access_token,
                              keypair=keypair)
        self._test_crud(api, file_name='test_api_wrapper.md')

    def _test_crud(self, api, file_name='test_crud.md'):
        folder_name = 'folder'
        subfolder_name = 'subfolder'
        body = '#hello Solid!'
        f = io.BytesIO(body.encode('UTF-8'))

        base_url = POD_ENDPOINT + 'playground-' + gen_random_str() + '/'
        folder_url = base_url + folder_name + '/'
        subfolder_url = folder_url + subfolder_name + '/'
        file_url = subfolder_url + file_name

        # This exists already
        self.assertTrue(api.item_exists(POD_ENDPOINT))

        # These do not exist yet
        self.assertFalse(api.item_exists(base_url))
        self.assertFalse(api.item_exists(folder_url))
        self.assertFalse(api.item_exists(subfolder_url))
        self.assertFalse(api.item_exists(file_url))

        # Read an existing file
        readme_file_url = POD_ENDPOINT + 'index.html'
        self.assertTrue(api.item_exists(readme_file_url))
        response = api.get(readme_file_url)
        print(f'index.html file contents: "{response.text}"')

        # Create new folders, non-existing parents are created automatically
        api.create_folder(subfolder_url)
        self.assertTrue(api.item_exists(folder_url))
        self.assertTrue(api.item_exists(subfolder_url))
        # folder = api.read_folder(folder_url)
        # self.assertIn(subfolder_name, folder.folders)

        empty_folder = api.read_folder(subfolder_url)
        self.assertIsInstance(empty_folder, FolderData)
        print(f'Folder name: {empty_folder.name}')
        print(f'Folder url: {empty_folder.url}')
        print(f'Parent: {empty_folder.parent}')
        print(f'Subfolders in the folder: {empty_folder.folders}')
        print(f'Files in the folder: {empty_folder.files}')

        # Create a new file
        api.put_file(file_url, f, 'text/plain')
        self.assertTrue(api.item_exists(file_url))
        folder_data = api.read_folder(subfolder_url)
        print(f'Files in the folder after creating file: {list(map(lambda x: x.name, folder_data.files))}')
        resp = api.get(file_url)
        print(f'{file_name} file contents: "{resp.text}"')
        self.assertTrue(body == resp.text, 'File contents does not match')

        # Clean up
        # Looks like one cannot remove folders with resources/containers in them,
        # so first one has to remove everything in child container before removing the parent container
        api.delete(file_url)
        api.delete_folder(subfolder_url)
        api.delete_folder(folder_url)
        api.delete_folder(base_url)
        self.assertFalse(api.item_exists(base_url))
        self.assertFalse(api.item_exists(folder_url))
        self.assertFalse(api.item_exists(subfolder_url))
        self.assertFalse(api.item_exists(file_url))


if __name__ == '__main__':
    unittest.main()
